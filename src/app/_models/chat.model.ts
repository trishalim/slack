import { Message } from './message.model';

export class Chat {

  // channel name for Channels
  // username for Direct Messages
  name: string;

  // messages in chat
  messages: Message[];

  // type of chat: Channel or Direct Message
  type: number;

  constructor(name: string, type: number) {
    this.name = name;
    this.type = type;
    this.messages = [];
  }
}
