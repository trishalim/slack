export class User {
  // unique identifier of user
  username: string;

  constructor(username: string) {
    this.username = username;
  }
}
