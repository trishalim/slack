export class Message {
  // the user who sent the message
  username: string;

  // content of the message
  content: string;

  // date the message was sent
  date: Date;

  constructor(username: string, content: string, date?: Date) {
    this.username = username;
    this.content = content;
    this.date = date ? date : new Date();
  }
}
