import { Injectable } from '@angular/core';

import { User } from '../_models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private loggedInUser: User;

  constructor() {
    this.loggedInUser = new User('trisha');
  }

  getLoggedInUser(): User {
    return this.loggedInUser;
  }

}
