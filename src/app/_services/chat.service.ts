import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';

import { Chat } from '../_models/chat.model';
import { Message } from '../_models/message.model';

import * as constants from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  // holds chats for both Channels and Direct Messages
  private chats: BehaviorSubject<Chat[]> = new BehaviorSubject([]);

  // currently opened chat
  private selectedChat: BehaviorSubject<Chat> = new BehaviorSubject(null);

  constructor() {
    // put in dummy chats or retrieve existing chats
    this.initChats();
  }

  initChats() {
    const localStorageChats = JSON.parse(localStorage.getItem('chats'));

    if (localStorageChats) {
      // retrieve existing chats in local storage
      this.chats.next(localStorageChats)
    } else {
      // there are no chats in local storage
      // create dummy chats
      const chats = [];
      chats.push(new Chat('general', constants.CHAT_TYPES.CHANNEL));
      chats[0].messages = [
        new Message('jane', 'Hi there!', new Date('11-8-2018')),
        new Message('ted', 'Hello', new Date('11-10-2018')),
        new Message('kate', 'What\'s up?'),
      ];

      chats.push(new Chat('sprints', constants.CHAT_TYPES.CHANNEL));
      chats[1].messages = [
        new Message('jane', 'Hello!', new Date('11-8-2018')),
        new Message('ted', 'Sprint planning later.'),
        new Message('kate', 'What time?'),
        new Message('jane', '5pm'),
        new Message('kate', 'See ya!'),
      ];

      chats.push(new Chat('random', constants.CHAT_TYPES.CHANNEL));
      chats[2].messages = [
        new Message('jane', 'Knock knock!'),
        new Message('ted', 'Who\'s there?'),
        new Message('jane', 'Carrie'),
        new Message('ted', 'Carrie who?'),
        new Message('jane', 'Carrie over to the next sprint.'),
      ];

      chats.push(new Chat('jane', constants.CHAT_TYPES.DIRECT_MESSAGE));
      chats[3].messages = [
        new Message('jane', 'Hello', new Date('11-8-2018')),
      ];

      chats.push(new Chat('ted', constants.CHAT_TYPES.DIRECT_MESSAGE));
      chats[4].messages = [
        new Message('ted', 'How are you?'),
      ];

      this.setChats(chats);
      this.updateLocalStorageChats();
    }

  }

  setChats(chats: Chat[]) {
    this.chats.next(chats);
  }

  getChats(): Observable<Chat[]> {
    return this.chats.asObservable();
  }

  setSelectedChat(chat: Chat) {
    this.selectedChat.next(chat);
  }

  getSelectedChat(): Observable<Chat> {
    return this.selectedChat.asObservable();
  }

  updateLocalStorageChats() {
    localStorage.setItem('chats', JSON.stringify(this.chats.value));
  }

  // add message to a chat
  sendMessage(chatName: string, chatType: number, username: string, content: string) {
    const message = new Message(username, content);
    const chats = this.chats.value;

    for (const chat of chats) {
      if (chat.name === chatName && chat.type === chatType) {
        // add message
        chat.messages.push(message);
        // update chat
        this.setSelectedChat(chat);
        break;
      }
    }

    this.setChats(chats);
    this.updateLocalStorageChats();
  }

  // create a new Channel or Direct Message
  addChat(chatName: string, type: number) {
    // remove white spaces
    chatName = chatName.trim();

    // add checking to avoid duplicate chat names in each chat type
    if (this.doesChatNameExist(chatName, type)) {
      alert(`Chat name "${chatName}" already exists. Please choose a different name.`);
      return false;
    }

    const chat = new Chat(chatName, type);
    const chats = this.chats.value;
    chats.push(chat);

    this.setChats(chats);
    this.updateLocalStorageChats();
    this.setSelectedChat(chat);

    return true;
  }

  // chat name should be unique under Channels and Direct Messages
  doesChatNameExist(chatName: string, chatType: number) {
    const chat = this.chats.value.find(chat => {
      return (chat.name === chatName && chat.type === chatType);
    });

    return chat ? true : false;
  }

  isSameChat(chat1: Chat, chat2: Chat) {
    return (chat1.name === chat2.name && chat1.type === chat2.type);
  }

}
