import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';

// Services
import { ChatService } from '../_services/chat.service';

// Models
import { Chat } from '../_models/chat.model';
import { User } from '../_models/user.model';

import * as constants from '../app.constants';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, OnChanges {

  @Input() chat: Chat;
  @Input() user: User;

  newMessage: string = '';
  appConstants = constants;

  constructor(private chatService: ChatService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('chat') &&
        changes.chat.currentValue && changes.chat.previousValue &&
        !this.chatService.isSameChat(changes.chat.currentValue, changes.chat.previousValue)) {
      this.newMessage = '';
    }
  }

  sendMessage() {
    this.chatService.sendMessage(this.chat.name, this.chat.type,
      this.user.username, this.newMessage);
    this.newMessage = '';
  }

  isSameDay(date1, date2) {
    date1 = new Date(date1);
    date2 = new Date(date2);
    return date1.getFullYear() === date2.getFullYear() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getDate() === date2.getDate();
  }

  isToday(date) {
    date = new Date(date);
    return (this.isSameDay(date, new Date()));
  }

}
