import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

// Services
import { ChatService } from '../_services/chat.service';

// Models
import { Chat } from '../_models/chat.model';

import * as constants from '../app.constants';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  @Input() chats: Chat[] = [];
  @Input() selectedChat: Chat;

  @Output() closeSideMenu = new EventEmitter<any>();

  newChannelName = '';
  showAddChannel = false;

  newDirectMessageName = '';
  showAddDirectMessage = false;

  appConstants = constants;

  constructor(private chatService: ChatService) { }

  ngOnInit() {
  }

  selectChat(chat: Chat) {
    this.chatService.setSelectedChat(chat);
    this.close();
  }

  toggleAddChannel() {
    this.showAddChannel = !this.showAddChannel;
  }

  addChannel() {
    const isSuccess = this.chatService.addChat(this.newChannelName, constants.CHAT_TYPES.CHANNEL);

    if (isSuccess) {
      this.showAddChannel = false;
    }
    this.newChannelName = '';
    this.close();
  }

  toggleAddDirectMessage() {
    this.showAddDirectMessage = !this.showAddDirectMessage;
  }

  addDirectMessage() {
    const isSuccess = this.chatService.addChat(this.newDirectMessageName, constants.CHAT_TYPES.DIRECT_MESSAGE);

    if (isSuccess) {
      this.showAddDirectMessage = false;
    }
    this.newDirectMessageName = '';
    this.close();
  }

  resetLocalStorage() {
    localStorage.clear();
    location.reload();
  }

  isChatSelected(chat: Chat) {
    return (this.selectedChat && chat.name === this.selectedChat.name
            && chat.type === this.selectedChat.type);
  }

  close() {
    this.closeSideMenu.emit();
  }

}
