import { Component } from '@angular/core';

// Services
import { ChatService } from './_services/chat.service';
import { UserService } from './_services/user.service';

// Models
import { Chat } from './_models/chat.model';
import { User } from './_models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  chats: Chat[] = [];
  selectedChat: Chat;
  loggedInUser: User;

  isSideMenuOpen = false;

  constructor(private chatService: ChatService,
              private userService: UserService) {
    chatService.getChats().subscribe(chats => {
      this.chats = chats;
    })
    chatService.getSelectedChat().subscribe(chat => {
      this.selectedChat = chat;
    })
    this.loggedInUser = userService.getLoggedInUser();
  }

  toggleSideMenu() {
    this.isSideMenuOpen = !this.isSideMenuOpen;
  }

}
