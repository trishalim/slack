import { Component, OnInit } from '@angular/core';

// Services
import { ChatService } from '../_services/chat.service';

import * as constants from '../app.constants';

@Component({
  selector: 'app-create-chat',
  templateUrl: './create-chat.component.html',
  styleUrls: ['./create-chat.component.scss']
})
export class CreateChatComponent implements OnInit {

  appConstants = constants;

  chatType = constants.CHAT_TYPES.CHANNEL;
  newChatName = '';

  constructor(private chatService: ChatService) { }

  ngOnInit() {
  }

  addChat() {
    this.chatService.addChat(this.newChatName, this.chatType);
  }

}
