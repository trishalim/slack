## URL
www.slack.trishalim.com

## Technologies
Angular, Bootstrap

## Scripts
### `ng serve`

Runs the app in development mode.

### `ng build --prod`

Builds the app for production to the `dist/` directory.

## Features
1. Create a chat and direct message.
2. Send messages in chat and direct message threads.
3. Saves messages in local storage.
4. Resets messages in local storage with the "Reset Local Storage" button on side bar.
5. Side bar collapses on mobile view.